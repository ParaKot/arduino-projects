#include <LiquidCrystal.h>  // библиотека для работы с дисплеем

LiquidCrystal lcd(8, 7, 9, 10, 11, 12); // (RS, E, DB4, DB5, DB6, DB7)
int light_border = 200; // пограничный уровень освещенности
int light_curent; // текущее освещение в помещении
float light_avg = 0; // среднее значение освещения по результатам цикла замера
float time_BL_enabled; // время в которое подсветка была включена
float time_temp; // временный счетчик времени для различных нужд
float time_mode1_block = 0; // время до которого будет заблокирована функция управления освещением по датчику освещенности
float start_delay_cycle; // время начала задержки обновления дисплея серии 1602
float time_BL_OnTime = 30000; // время подсветки дисплея по-умолчанию 30 секунд
int mode = 1; // режим срабатывания (1 - по уровню освещенности, 2 - по времени)
int button_1, button_2; //имена кнопок
int i; // счетчик шагов цикла
int menu_selected = 0; // ID текущего выбранного пункта меню


void displayBL() {
  // функция управляет подсветкой экрана
  if(millis() - time_BL_enabled > time_BL_OnTime) {
    digitalWrite(13, LOW); // выключаем подсветку
  }
  else {
    digitalWrite(13, HIGH); // включаем подсветку
  }
}

void setup(){
  Serial.begin(9600);
  pinMode(13, OUTPUT); // подсветка дисплея
  pinMode(5, OUTPUT); // реле
  pinMode(3, INPUT); // 1-я кнопка
  pinMode(2, INPUT); // 2-я кнопка

  lcd.begin(16, 2); // Задаем размерность экрана
  lcd.setCursor(0, 0);
  lcd.print("Light controller");
  lcd.setCursor(0, 1);
  lcd.print("by Yauhen.ORG");
  delay(1500);
  lcd.clear();
}

void loop(){
  displayBL();
  light_curent = analogRead(A5);
  
  switch(mode) {
    case 1:
      if(millis() > time_mode1_block) {
        /*
         * функция управления освещением по уровню освещенности работает только в том случае,
         * если освещение небыло включено/выключено принудительно, либо таймаут с момента
         * такого переключения истек
         */
        time_mode1_block = 0;
        if(light_avg <= light_border) {
          digitalWrite(5, LOW);
        }
        if(light_avg > light_border) {
          digitalWrite(5, HIGH);
        }
        break;
      }
    case 2:
      break;
  }

  /*
   * Работа с меню
   */
  if((button_1 or button_2) == HIGH) {
    time_BL_enabled = millis(); // активируем подсветку
    displayBL();
    if(button_1 == HIGH) {
      
    }

    // кнопкой 2 можно всегда включить либо выключить свет вручную
    if((button_2 == HIGH) and (menu_selected == 0)) {
      if(digitalRead(5) == HIGH) {
        // выключаем свет с задеркой в 30 секунд (помощь в темноте)
        time_temp = millis();
        lcd.setCursor(0, 0);
        lcd.print("The light turns");
        lcd.setCursor(1, 1);
        lcd.print("off in ");
        while(millis() - time_temp < 30000) {
          lcd.print("          ");
          lcd.setCursor(9, 1);
          lcd.print(30 - (int)floor((millis() - time_temp)/1000));
          lcd.print(" sec.");
          lcd.setCursor(9, 1);
          delay(975);
        }
        digitalWrite(5, LOW);
        time_mode1_block = millis() + 28800000; // таймаут удержания в ручном режиме равен 8 часам
      }
      else {
        digitalWrite(5, HIGH);
        time_mode1_block = millis() + 10800000; // таймаут удержания в ручном режиме равен 3 часам
      }
    }
  }
  else {
    if(menu_selected == 0) {
      // вывод главного экрана системы
      lcd.print("Voltage:");
      lcd.print(light_avg*0.0048);
    }
  }
  
  /*
   * ЦИКЛ ЗАДЕРЖКИ ОБНОВЛЕНИЯ ДИСПЛЕЯ
   * - в ходе работы собирает данные с датчиков и кнопок
   * - о завершению отдает актуальную информацию о собранных данных
   */
  button_1 = LOW;
  button_2 = LOW;
  i = 0;
  light_avg = 0;
  start_delay_cycle = millis();
  while(start_delay_cycle + 500 > millis()) {
    light_curent = analogRead(A5);
    // за одну итерацию задержки (полное прохождение цикла) можно передать только одна событие нажатия кнопки
    if((digitalRead(3) == HIGH) and (button_2 != HIGH)) {
      button_1 = HIGH;
    }
    if((digitalRead(2) == HIGH) and (button_1 != HIGH)) {
      button_2 = HIGH;
    }
    light_avg = light_avg + light_curent;
    i++;
  }
  light_avg = light_avg / i;
  lcd.clear();
}
