#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

// pin 2 - Serial clock out (SCLK)
// pin 3 - Serial data out (DIN)
// pin 4 - Data/Command select (D/C)
// pin 5 - LCD chip select (CS)
// pin 6 - LCD reset (RST)
// pin 1 - display backlight

Adafruit_PCD8544 display = Adafruit_PCD8544(2, 3, 4, 5, 6);

// набор разнозадачных переменных
float start_time, up_s, up_m, up_h;
float bl_time_s;
int menu_pos_active, lmenu_pos_active;
bool button_lock;

// таймер
float timer_h, timer_m;
int timer_select; // 1 - настроить часы, 2 - настроить минуты

// инициализируем состояние кнопок
int button_1, button_2, button_3, button_4;

// инициализируем состояние прочих датчиков
int poweredON;

// иконки
const unsigned char hdd_no[] PROGMEM = {
    0x3F, 0xF8, //   ###########
    0x40, 0x04, //  #           #
    0xAB, 0x32, // # # # ##  ##  #
    0xAA, 0xAA, // # # # # # # # #
    0xBA, 0xAA, // # ### # # # # #
    0xAA, 0xAA, // # # # # # # # #
    0xAB, 0x32, // # # # ##  ##  #
    0x40, 0x04, //  #           #
    0x3F, 0xF8, //   ###########
};
const unsigned char hdd_yes[] PROGMEM = {
    0x3F, 0xF8, //   ###########
    0x7F, 0xFC, //  #############
    0xD4, 0xCE, // ## # #  ##  ###
    0xD5, 0x56, // ## # # # # # ##
    0xC5, 0x56, // ##   # # # # ##
    0xD5, 0x56, // ## # # # # # ##
    0xD4, 0xCE, // ## # #  ##  ###
    0x7F, 0xFC, //  #############
    0x3F, 0xF8, //   ###########
};

void print_menu_item(char menu_text[], int menu_pos, int menu_pos_active) {
    display.setCursor(0, 11 * menu_pos + 1);
    if (menu_pos_active == menu_pos) {
        // выделяем пункт меню
        display.setTextColor(WHITE, BLACK);
    }
    display.print(menu_text);
    display.setTextColor(BLACK);
    display.setCursor(0, 0);
}

void shutdown_PC() {
    display.clearDisplay();
    // выключаем ПК
    digitalWrite(11, LOW);
    display.setCursor(0, 20);
    display.setTextSize(2);
    display.print("STOPING");
    display.display();
    delay(500);
    digitalWrite(11, HIGH);
    int shutdown_timer = millis();
    while (digitalRead(12) == HIGH) {
        /*
          ПК должен самостоятельно выключиться в течении 30 секунд, иначе будет выключен принудительно
        */
        if (millis() - shutdown_timer > 60000) {
            // принудительное выключение питания
            digitalWrite(11, LOW);
            delay(6000);
        }
    }
    digitalWrite(11, HIGH);
    display.setCursor(0, 0);
    display.setTextSize(1);
}

void start_PC() {
    display.clearDisplay();
    // включаем ПК
    digitalWrite(11, LOW);
    start_time = millis();
    display.setCursor(0, 20);
    display.setTextSize(2);
    display.print("RUNING");
    display.display();
    delay(500);
    digitalWrite(11, HIGH);
    display.setCursor(0, 0);
    display.setTextSize(1);
    delay(1000);
}

void setup() {
    start_time = 0;
    menu_pos_active = 0;
    lmenu_pos_active = 0;
    bl_time_s = millis();
    button_lock = false;
    timer_h = 0;
    timer_m = 0;
    timer_select = 0;

    pinMode(0, OUTPUT); // питание подсветки дисплея
    digitalWrite(0, HIGH);

    pinMode(12, INPUT); // статус работы ПК
    pinMode(13, INPUT); // статус работы HDD
    pinMode(11, OUTPUT); // запуск ПК
    digitalWrite(11, HIGH);

    display.begin();              // Инициализация дисплея
    display.setContrast(60);      // Устанавливаем контраст

    display.clearDisplay();       // Очищаем дисплей
    display.display();
    //delay(1000);
}

void loop() {
    pinMode(7, OUTPUT); // кнопка 3
    pinMode(8, OUTPUT); // кнопка 1
    pinMode(9, OUTPUT); // кнопка 4
    pinMode(10, OUTPUT); // кнопка 2

    button_1 = digitalRead(8);
    button_2 = digitalRead(10);
    button_3 = digitalRead(7);
    button_4 = digitalRead(9);

    // обработка подсветки дисплея
    if ((button_1 or button_2 or button_3 or button_4) == HIGH) {
        bl_time_s = millis();
        digitalWrite(0, HIGH); // включаем подсветку дисплея

        if (timer_select == 0) {
            // обработка перемещения по меню
            if ((button_1 == HIGH) and (button_lock != true)) {
                // движение по меню вверх
                if (menu_pos_active == 0) {
                    menu_pos_active = 1;
                }
                else {
                    if (menu_pos_active == 1) {
                        menu_pos_active = 3;
                    }
                    else {
                        menu_pos_active = menu_pos_active - 1;
                    }
                }
            }
            if ((button_2 == HIGH) and (button_lock != true)) {
                // движение по меню вниз
                if (menu_pos_active == 0) {
                    menu_pos_active = 1;
                }
                else {
                    if (menu_pos_active == 3) {
                        menu_pos_active = 1;
                    }
                    else {
                        menu_pos_active = menu_pos_active + 1;
                    }
                }
            }
            button_lock = true;
        }
    }
    else {
        button_lock = false;
        if (millis() - bl_time_s > 10000) {
            digitalWrite(0, LOW); // выключаем подсветку дисплея
        }
        else {
            digitalWrite(0, HIGH);
        }
    }

    poweredON = digitalRead(12);

    // Выводим текущие значения параметров
    display.clearDisplay();
    display.setCursor(0, 0);
    display.setTextColor(BLACK);  // Устанавливаем цвет текста
    display.setTextSize(1);       // Устанавливаем размер текста

    if (poweredON == HIGH) {
        // если ПК включен
        if (start_time == 0) {
          start_time = millis();
        }
        display.drawLine(0, 10, 84, 10, BLACK); // горизонтальный разделитель
        display.drawLine(67, 0, 67, 9, BLACK); // вертикальный разделитель

        if ((button_4 == HIGH) and (menu_pos_active != 0) and (timer_select == 0)) {
            while (digitalRead(9) == HIGH) {
              // ожидаем отпускание кнопки 4
            }
            // выполняем пункт меню
            if (menu_pos_active == 1) {
                // настройка таймера выключения
                timer_select = 1;
            }
            if (menu_pos_active == 2) {
                // перезагружаем ПК
                shutdown_PC();
                delay(1000);
                start_PC();
            }
            if (menu_pos_active == 3) {
                // выключаем ПК
                shutdown_PC();
            }
        }
        else {
            if (timer_select != 0) {
                // выключение ПК по таймеру
                display.setCursor(0,12);
                display.println("ShutDown Timer");
                display.setTextSize(2);

                if (timer_select == 1) {
                    display.setTextColor(WHITE, BLACK);
                    if (button_1 == HIGH) {
                        if (timer_h == 23) {
                            timer_h = 0;
                        }
                        else {
                            timer_h = timer_h + 1;
                        }
                        delay(300);
                    }
                    if (button_2 == HIGH) {
                        if (timer_h == 0) {
                            timer_h = 23;
                        }
                        else {
                            timer_h = timer_h - 1;
                        }
                        delay(300);
                    }

                    // отмена установки таймера
                    if (button_3 == HIGH) {
                        timer_select = 0;
                        while (digitalRead(7) == HIGH) {
                            // ожидаем пока не будет отпущена кнопка 3
                        }
                    }

                    // подтверждение ввода часов
                    if (button_4 == HIGH) {
                        timer_select = 2;
                        while (digitalRead(9) == HIGH) {
                            // ожидаем пока не будет отпущена кнопка 4
                        }
                    }
                }

                if (timer_h < 10) {
                    display.print("0");
                }
                display.print((int)(timer_h));
                display.setTextColor(BLACK);
                display.print(":");

                if (timer_select == 2) {
                    display.setTextColor(WHITE, BLACK);
                    if (button_1 == HIGH) {
                        if (timer_m == 59) {
                            timer_m = 0;
                        }
                        else {
                            timer_m = timer_m + 1;
                        }
                        delay(300);
                    }
                    if (button_2 == HIGH) {
                        if (timer_m == 0) {
                            timer_m = 59;
                        }
                        else {
                            timer_m = timer_m - 1;
                        }
                        delay(300);
                    }

                    // возврат к настройке часов
                    if (button_3 == HIGH) {
                        timer_select = 1;
                        while (digitalRead(7) == HIGH) {
                            // ожидаем пока не будет отпущена кнопка 3
                        }
                    }

                    // запуск таймера
                    if (digitalRead(9) == HIGH) {
                        display.setTextColor(BLACK);
                        timer_select = 0;
                        while (digitalRead(9) == HIGH) {
                            // ожидаем пока не будет отпущена кнопка 4
                        }
                        
                        float go_time = millis() + (timer_h * 3600 * 1000) + (timer_m * 60 * 1000);
                        float th, tm, ts; // отображаемые часы и минуты обратного отсчета
                        while (millis() < go_time) {
                            display.clearDisplay();
                            display.setCursor(0,0);
                            // выводим таймер обратного отсчета запуска
                            display.setTextSize(1);
                            display.println("Shutdown in:");
                            display.println();
                            display.setTextSize(2);
                            th = ((go_time - millis()) / 1000) / 3600;
                            if (th < 10) {
                                display.print("0");
                            }
                            display.print((int)(th));
                            display.print(":");
                            
                            tm = ((go_time - millis()) - (floor(th) * 60 * 60 *1000)) / 60000;
                            if (tm < 10) {
                                display.print("0");
                            }
                            display.print((int)(tm));
                            
                            display.setTextSize(1);
                            display.print(":");

                            ts = ((((go_time - millis()) - (floor(tm) * 60000) - (floor(th) * 3600000)) / 1000));
                            if (ts < 10) {
                                display.print("0");
                            }
                            display.println((int)ts);

                            display.display();

                            if (digitalRead(7) == HIGH) {
                                // если нажата кнопка 3, то сбрасываем таймер запуска
                                while (digitalRead(7) == HIGH) {
                                  // ожидаем отпускание кнопки 3
                                }
                                break;
                            }
                        }
                        if (millis() >= go_time) {
                            // выключение ПК
                            shutdown_PC();
                        }
                    }
                }

                if (timer_m < 10) {
                    display.print("0");
                }
                display.print((int)(timer_m));
                display.setTextColor(BLACK);
                display.setCursor(0,0);
                display.setTextSize(1);
            }
            else {
                print_menu_item("PowerOff Timer", 1, menu_pos_active);
                print_menu_item("Reboot", 2, menu_pos_active);
                print_menu_item("Shutdown", 3, menu_pos_active);
            }
        }
        
        up_h = ((millis() - start_time) / 1000) / 3600;
        up_m  = ((millis() - start_time) - (floor(up_h) * 60 * 60 *1000)) / 60000;
        up_s = ((((millis() - start_time) - (floor(up_m) * 60000) - (floor(up_h) * 3600000)) / 1000));

        if (up_h < 10) {
            display.print("0");
        }
        display.print((int)(up_h));
        display.print(":");
        if (up_m < 10) {
            display.print("0");
        }
        display.print((int)(up_m));
        display.print(":");
        if (up_s < 10) {
            display.print("0");
        }
        display.print((int)(up_s));

        if (digitalRead(13) == HIGH) {
            display.drawBitmap(69, 0, hdd_yes, 15, 9, 1);
        }
        else {
            display.drawBitmap(69, 0, hdd_no, 15, 9, 1);
        }
    }
    else {
        // если ПК выключен
        start_time = 0; // сбор времени запуска ПК
        if ((button_4 == HIGH) and (menu_pos_active != 0) and (timer_select == 0)) {
            while (digitalRead(9) == HIGH) {
                // ожидаем пока не будет отпущена кнопка 4
            }
            // выполняем пункт меню
            if (menu_pos_active == 1) {
                // включение ПК
                start_PC();
            }
            if (menu_pos_active == 2) {
                timer_select = 1;
            }
        }
        else {
            if (timer_select != 0) {
                // включение ПК по таймеру
                display.println("StartUp Timer");
                display.setTextSize(2);

                if (timer_select == 1) {
                    display.setTextColor(WHITE, BLACK);
                    if (button_1 == HIGH) {
                        if (timer_h == 23) {
                            timer_h = 0;
                        }
                        else {
                            timer_h = timer_h + 1;
                        }
                        delay(300);
                    }
                    if (button_2 == HIGH) {
                        if (timer_h == 0) {
                            timer_h = 23;
                        }
                        else {
                            timer_h = timer_h - 1;
                        }
                        delay(300);
                    }

                    // отмена установки таймера
                    if (button_3 == HIGH) {
                        timer_select = 0;
                        while (digitalRead(7) == HIGH) {
                            // ожидаем пока не будет отпущена кнопка 3
                        }
                    }

                    // подтверждение ввода часов
                    if (button_4 == HIGH) {
                        timer_select = 2;
                        while (digitalRead(9) == HIGH) {
                            // ожидаем пока не будет отпущена кнопка 4
                        }
                    }
                }

                if (timer_h < 10) {
                    display.print("0");
                }
                display.print((int)(timer_h));
                display.setTextColor(BLACK);
                display.print(":");

                if (timer_select == 2) {
                    display.setTextColor(WHITE, BLACK);
                    if (button_1 == HIGH) {
                        if (timer_m == 59) {
                            timer_m = 0;
                        }
                        else {
                            timer_m = timer_m + 1;
                        }
                        delay(300);
                    }
                    if (button_2 == HIGH) {
                        if (timer_m == 0) {
                            timer_m = 59;
                        }
                        else {
                            timer_m = timer_m - 1;
                        }
                        delay(300);
                    }

                    // возврат к настройке часов
                    if (button_3 == HIGH) {
                        timer_select = 1;
                        while (digitalRead(7) == HIGH) {
                            // ожидаем пока не будет отпущена кнопка 3
                        }
                    }

                    // запуск таймера
                    if (digitalRead(9) == HIGH) {
                        display.setTextColor(BLACK);
                        timer_select = 0;
                        while (digitalRead(9) == HIGH) {
                            // ожидаем пока не будет отпущена кнопка 4
                        }
                        
                        float go_time = millis() + (timer_h * 3600 * 1000) + (timer_m * 60 * 1000);
                        float th, tm, ts; // отображаемые часы и минуты обратного отсчета
                        while (millis() < go_time) {
                            display.clearDisplay();
                            display.setCursor(0,0);
                            // выводим таймер обратного отсчета запуска
                            display.setTextSize(1);
                            display.println("Starting in:");
                            display.println();
                            display.setTextSize(2);
                            th = ((go_time - millis()) / 1000) / 3600;
                            if (th < 10) {
                                display.print("0");
                            }
                            display.print((int)(th));
                            display.print(":");
                            
                            tm = ((go_time - millis()) - (floor(th) * 60 * 60 *1000)) / 60000;
                            if (tm < 10) {
                                display.print("0");
                            }
                            display.print((int)(tm));
                            
                            display.setTextSize(1);
                            display.print(":");

                            ts = ((((go_time - millis()) - (floor(tm) * 60000) - (floor(th) * 3600000)) / 1000));
                            if (ts < 10) {
                                display.print("0");
                            }
                            display.println((int)ts);

                            display.display();

                            if (digitalRead(7) == HIGH) {
                                // если нажата кнопка 3, то сбрасываем таймер запуска
                                while (digitalRead(7) == HIGH) {
                                  // ожидаем отпускание кнопки 3
                                }
                                break;
                            }
                        }
                        if (millis() >= go_time) {
                        // включение ПК
                        start_PC();
                    }
                    }
                }

                if (timer_m < 10) {
                    display.print("0");
                }
                display.print((int)(timer_m));
                display.setTextColor(BLACK);
            }
            else {
                display.println("Select action:");
                display.drawLine(0, 10, 84, 10, BLACK); // горизонтальный разделитель
                print_menu_item("Start system", 1, menu_pos_active);
                print_menu_item("StartUp Timer", 2, menu_pos_active);
            }
        }
    }

    display.display();
}
